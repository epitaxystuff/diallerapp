//
//  BaseViewController.m
//  DiallerApp
//
//  Created by Valerii Lider on 4/21/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "BaseViewController.h"

#import "AppDelegate.h"

typedef void (^AlertActionBlock)(NSInteger buttonIndex, NSString *buttonTitle);

@interface BaseViewController () <UIAlertViewDelegate, UIActionSheetDelegate>

@property (nonatomic, copy) AlertActionBlock alertBlock;

@end

@implementation BaseViewController

- (void)dealloc
{
    self.loader = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Public Methods

- (void)showProcessignIndicator
{
    if (nil != self.loader.superview)
        return;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.window.userInteractionEnabled = NO;
    CGPoint center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    if (nil == self.loader)
        self.loader = [[Loader alloc] initWithSize:90.f center:center];
    [self.loader startAnimating];
    [self.view addSubview:self.loader];
}

- (void)hideProcessingIndicator
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.window.userInteractionEnabled = YES;
    [self.loader stopAnimating];
    [self.loader removeFromSuperview];
}

- (void)showActionAlert:(BOOL)isAlert withTitle:(NSString *)title body:(NSString *)body cancelButton:(NSString *)cancelButton otherButtons:(NSArray *)otherButtons completion:(void(^)(NSInteger buttonIndex, NSString *buttonTitle))completion
{
    if (nil != self.alertBlock)
        self.alertBlock = nil;
    if (completion)
        self.alertBlock = completion;
    
    if ([UIAlertController class]) {
        
        UIAlertController *alertSheet = [UIAlertController alertControllerWithTitle:title
                                                                            message:body
                                                                     preferredStyle:(isAlert ? UIAlertControllerStyleAlert : UIAlertControllerStyleActionSheet)];
        if (!isAlert && UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
            
            alertSheet.modalPresentationStyle = UIModalPresentationPopover;
            alertSheet.popoverPresentationController.barButtonItem = nil;
            alertSheet.popoverPresentationController.sourceView = self.view;
        } else
            alertSheet.modalPresentationStyle = UIModalPresentationFormSheet;
        
        __weak __typeof(self) weakSelf = self;
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButton style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf.alertBlock)
                strongSelf.alertBlock(kAlertCancelButtonIndex, action.title);
        }];
        [alertSheet addAction:cancelAction];
        
        for (NSInteger i = 0; i < otherButtons.count; ++i) {
            
            UIAlertAction *otherAction = [UIAlertAction actionWithTitle:otherButtons[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf.alertBlock)
                    strongSelf.alertBlock(i+1, action.title);
            }];
            [alertSheet addAction:otherAction];
        }
        [self presentViewController:alertSheet animated:YES completion:NULL];
    } else {
        
        if (isAlert) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                            message:body
                                                           delegate:self
                                                  cancelButtonTitle:cancelButton
                                                  otherButtonTitles:nil];
            for (NSString *buttonTitle in otherButtons)
                [alert addButtonWithTitle:buttonTitle];
            [alert show];
        } else {
            
            UIActionSheet *sheet = [[UIActionSheet alloc] init];
            sheet.delegate = self;
            sheet.title = title;
            for (NSString *buttonTitle in otherButtons)
                [sheet addButtonWithTitle:buttonTitle];
            sheet.cancelButtonIndex = [sheet addButtonWithTitle:cancelButton];
            [sheet showInView:self.view];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.alertBlock)
        self.alertBlock(buttonIndex, [alertView buttonTitleAtIndex:buttonIndex]);
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.alertBlock) {
        
        if (actionSheet.cancelButtonIndex == buttonIndex)
            self.alertBlock(kAlertCancelButtonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
        else
            self.alertBlock(buttonIndex+1, [actionSheet buttonTitleAtIndex:buttonIndex]);
    }
}

@end
