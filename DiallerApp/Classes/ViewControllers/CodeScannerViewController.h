//
//  CodeScannerViewController.h
//  DiallerApp
//
//  Created by Valerii Lider on 4/8/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CodeScannerViewControllerDelegate <NSObject>

@required
- (void)onDidCaptureCode:(NSString *)code;

@end

@interface CodeScannerViewController : UIViewController

@property (nonatomic, assign) id <CodeScannerViewControllerDelegate> delegate;

@end
