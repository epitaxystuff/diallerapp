//
//  BaseViewController.h
//  DiallerApp
//
//  Created by Valerii Lider on 4/21/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Loader.h"

#define kAlertCancelButtonIndex                 0

@interface BaseViewController : UIViewController

@property (nonatomic, strong) Loader *loader;

- (void)showProcessignIndicator;
- (void)hideProcessingIndicator;
- (void)showActionAlert:(BOOL)isAlert withTitle:(NSString *)title body:(NSString *)body cancelButton:(NSString *)cancelButton otherButtons:(NSArray *)otherButtons completion:(void(^)(NSInteger buttonIndex, NSString *buttonTitle))completion;

@end
