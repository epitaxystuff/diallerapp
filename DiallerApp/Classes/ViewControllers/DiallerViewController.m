//
//  DialerViewController.m
//  DialerApp
//
//  Created by Valerii Lider on 4/7/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "DiallerViewController.h"
#import "PadNumberView.h"
#import "ContactCell.h"
#import "AddressBookFetcher.h"
#import <AddressBookUI/AddressBookUI.h>

#define kPadButtonHeight            50
#define kMaxNumberLength            14

typedef enum {
    SelectCountryTypeFrom = 1,
    SelectCountryTypeTo
} SelectCountryType;

@interface DiallerViewController () <ABPeoplePickerNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, PadNumberViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel *fromLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *fromLabelWidth;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *fromViewWidth;
@property (nonatomic, weak) IBOutlet UILabel *toLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *toLabelWidth;
@property (nonatomic, weak) IBOutlet UILabel *numberLabel;
@property (nonatomic, weak) IBOutlet UIView *padView;
@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, weak) IBOutlet UIButton *startCallButton;
@property (nonatomic, weak) IBOutlet UITextField *fakeTextField;
@property (nonatomic, strong) UIPickerView *countriesPicker;

@property (nonatomic, assign) SelectCountryType countryType;
@property (nonatomic, strong) NSArray *countries;
@property (nonatomic, strong) NSMutableArray *contactsDisplay;

@end

@implementation DiallerViewController

- (void)dealloc
{
    self.countries = nil;
    self.contactsDisplay = nil;
    self.countriesPicker = nil;
    self.loginID = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.countries = @[@{kCountryNameKey:@"Australia", kCountryCodeKey:@"AU"},
                       @{kCountryNameKey:@"Hong Kong", kCountryCodeKey:@"HK"},
                       @{kCountryNameKey:@"Israel", kCountryCodeKey:@"IL"},
                       @{kCountryNameKey:@"UK", kCountryCodeKey:@"UK"},
                       @{kCountryNameKey:@"USA", kCountryCodeKey:@"US"}];
    self.contactsDisplay = [@[] mutableCopy];
    
    //Set UI
    self.navigationController.navigationBarHidden = YES;
    {
        NSString *selectedCode = [defaults objectForKey:kLastFromCountryCode];
        for (NSInteger i = 0; i < self.countries.count; ++i) {
            
            NSDictionary *country = self.countries[i];
            NSString *countryCode = country[kCountryCodeKey];
            if ([countryCode isEqualToString:selectedCode]) {
                self.fromLabel.text = country[kCountryNameKey];
                break;
            }
        }
    }
    {
        NSString *selectedCode = [defaults objectForKey:kLastToCountryCode];
        for (NSInteger i = 0; i < self.countries.count; ++i) {
            
            NSDictionary *country = self.countries[i];
            NSString *countryCode = country[kCountryCodeKey];
            if ([countryCode isEqualToString:selectedCode]) {
                self.toLabel.text = country[kCountryNameKey];
                break;
            }
        }
    }
    for (PadNumberView *padNumber in self.padView.subviews)
        padNumber.delegate = self;
    
    self.fromViewWidth.constant = 145*kScreenWidth/320;
    self.fromLabel.font = [UIFont boldSystemFontOfSize:13.f*kScreenWidth/320];
    self.toLabel.font = [UIFont boldSystemFontOfSize:13.f*kScreenWidth/320];
    [self fetchCountriesLabelsSizes];
    self.numberLabel.text = @"";
    
    self.countriesPicker = [[UIPickerView alloc] init];
    self.countriesPicker.dataSource = self;
    self.countriesPicker.delegate = self;
    self.countriesPicker.showsSelectionIndicator = YES;
    self.fakeTextField.inputView = self.countriesPicker;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.f, 0.f, self.view.bounds.size.width, 44.f)];
    pickerToolbar.items = @[[[UIBarButtonItem alloc] initWithTitle:@"Cancel".localize
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(didCancelCountriesPicker:)],
                            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:nil
                                                                          action:nil],
                            [[UIBarButtonItem alloc] initWithTitle:@"Done".localize
                                                             style:UIBarButtonItemStyleDone
                                                            target:self
                                                            action:@selector(didDoneCountriesPicker:)]];
    self.fakeTextField.inputAccessoryView = pickerToolbar;
    
    [self showProcessignIndicator];
    __weak __typeof(self) weakSelf = self;
    [[AddressBookFetcher sharedObject] loadContactsWithCompletion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf hideProcessingIndicator];
        });
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)didPressFromLabel:(UITapGestureRecognizer *)sender
{
    self.countryType = SelectCountryTypeFrom;
    [self showCountriesPicker];
}

- (IBAction)didPressToLabel:(UITapGestureRecognizer *)sender
{
    self.countryType = SelectCountryTypeTo;
    [self showCountriesPicker];
}

- (IBAction)didPressBackspaceButton:(UIButton *)sender
{
    if (0 != self.numberLabel.text.length) {
        self.numberLabel.text = [self.numberLabel.text substringToIndex:self.numberLabel.text.length-1];
        [self searchContacts];
    }
}

- (IBAction)didPressContactsButton:(UIButton *)sender
{
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    [self.navigationController presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)didPressStartCallButton:(UIButton *)sender
{
    if ([@"" isEqualToString:self.numberLabel.text]) {
        [self showActionAlert:YES
                    withTitle:@"Warning".localize
                         body:@"Number field should not be empty".localize
                 cancelButton:@"Cancel".localize
                 otherButtons:nil
                   completion:NULL];
    } else {
        
        __weak __typeof(self) weakSelf = self;
        [self showProcessignIndicator];
        NSString *fromCode = [[NSUserDefaults standardUserDefaults] objectForKey:kLastFromCountryCode];
        NSString *toCode = [[NSUserDefaults standardUserDefaults] objectForKey:kLastToCountryCode];
        NSString *url = [NSString stringWithFormat:@"https://sia.wikistrat.com/openvbx/ivr/callboxed/endpoint.php?u=%@&f=%@&t=%@&d=%@", self.loginID, fromCode.lowercaseString, toCode.lowercaseString, self.numberLabel.text];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"GET"];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            if (nil == connectionError && nil != data) {
                //TODO:
            } else {
                
                [self showActionAlert:YES
                            withTitle:@"Warning".localize
                                 body:@"Could not make call".localize
                         cancelButton:@"OK"
                         otherButtons:nil
                           completion:NULL];
            }
            [strongSelf hideProcessingIndicator];
        }];
    }
}

- (IBAction)didPressMenuButton:(UIButton *)sender
{
    
}

#pragma mark - PadNumberView Delegate

- (void)padNumberTappedWithTag:(NSInteger)tag isLongPress:(BOOL)longPress
{
    if (kMaxNumberLength > self.numberLabel.text.length) {
        
        switch (tag) {
            case PadButtonTagAsterisk:
                self.numberLabel.text = [self.numberLabel.text stringByAppendingString:@"*"];
                break;
            case PadButtonTagZero:
                self.numberLabel.text = [self.numberLabel.text stringByAppendingString:@"0"];
                break;
            case PadButtonTagSharp:
                self.numberLabel.text = [self.numberLabel.text stringByAppendingString:@"#"];
                break;
            default:
                self.numberLabel.text = [self.numberLabel.text stringByAppendingString:[@(tag) stringValue]];
                break;
        }
        if (!longPress)
            [self searchContacts];
    }
}

- (void)padNumberLongPressed
{
    NSString *text = self.numberLabel.text;
    if (kMaxNumberLength > text.length) {
        if ([@"0" isEqualToString:[text substringFromIndex:text.length-1]]) {
            text = [text substringToIndex:text.length-1];
            self.numberLabel.text = [text stringByAppendingString:@"+"];
        } else
            self.numberLabel.text = [self.numberLabel.text stringByAppendingString:@"+"];
        [self searchContacts];
    }
}

- (void)searchContacts
{
    [self.contactsDisplay removeAllObjects];
    if ([@"" isEqualToString:self.numberLabel.text])
        self.startCallButton.enabled = NO;
    else {
        self.startCallButton.enabled = YES;
        for (NSDictionary *person in [AddressBookFetcher sharedObject].contacts) {
            NSArray *phones = person[kContactPhonesKey];
            for (NSString *phone in phones) {
                if (NSNotFound != [phone rangeOfString:self.numberLabel.text].location)
                    [self.contactsDisplay addObject:person];
            }
        }
    }
    [self.table reloadData];
}

#pragma mark - Table Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contactsDisplay.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *contactCellIdentifier = @"ContactCellIdentifier";
    ContactCell *cell = [tableView dequeueReusableCellWithIdentifier:contactCellIdentifier];
    NSDictionary *contact = self.contactsDisplay[indexPath.row];
    if (contact[kContactAvatarKey])
        cell.avatar.image = contact[kContactAvatarKey];
    else
        cell.avatar.image = [UIImage imageNamed:@"NoAvatarIcon"];
    cell.contactNameLabel.text = contact[kContactNameKey];
    NSArray *phones = contact[kContactPhonesKey];
    cell.contactPhoneLabel.text = phones.firstObject;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSDictionary *contact = self.contactsDisplay[indexPath.row];
    NSArray *phones = contact[kContactPhonesKey];
    [self didSelectContactWithNumbers:phones];
}

#pragma mark - Private Methods

- (void)showCountriesPicker
{
    NSString *selectedCode = (SelectCountryTypeFrom == self.countryType ? [[NSUserDefaults standardUserDefaults] objectForKey:kLastFromCountryCode] : [[NSUserDefaults standardUserDefaults] objectForKey:kLastToCountryCode]);
    for (NSInteger i = 0; i < self.countries.count; ++i) {
        
        NSDictionary *country = self.countries[i];
        NSString *countryCode = country[kCountryCodeKey];
        if ([countryCode isEqualToString:selectedCode]) {
            [self.countriesPicker selectRow:i inComponent:0 animated:NO];
            break;
        }
    }
    [self.fakeTextField becomeFirstResponder];
}

- (void)fetchCountriesLabelsSizes
{
    {
        CGSize textSize = [self.fromLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, self.fromLabel.bounds.size.height)
                                                            options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                         attributes:@{NSFontAttributeName:self.fromLabel.font} context:nil].size;
        self.fromLabelWidth.constant = textSize.width+1.f;
    }
    {
        CGSize textSize = [self.toLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, self.toLabel.bounds.size.height)
                                                            options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                         attributes:@{NSFontAttributeName:self.toLabel.font} context:nil].size;
        self.toLabelWidth.constant = textSize.width+1.f;
    }
}

#pragma mark - Address Book Delegate

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person
{
    [self didSelectContact:person];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    [self didSelectContact:person];
    
    return NO;
}

- (void)didSelectContact:(ABRecordRef)person
{
    NSMutableArray *personPhoneNumbers = [@[] mutableCopy];
    
    ABMultiValueRef multiPhonesRef = ABRecordCopyValue(person, kABPersonPhoneProperty);
    if (multiPhonesRef) {
        
        if (0 != ABMultiValueGetCount(multiPhonesRef)) {
            
            for (NSInteger i = 0; i < ABMultiValueGetCount(multiPhonesRef); ++i) {
                
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhonesRef, i);
                NSString *phoneNumber = (__bridge NSString *)phoneNumberRef;
                if (![@"" isEqualToString:phoneNumber]) {
                    
                    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@" -()"];
                    phoneNumber = [[phoneNumber componentsSeparatedByCharactersInSet:doNotWant] componentsJoinedByString:@""];
                    phoneNumber = [phoneNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
                    NSString *phoneRegex = @"^(\\+?)(\\d{1,17})$";
                    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                    if ([phoneTest evaluateWithObject:phoneNumber])
                        [personPhoneNumbers addObject:phoneNumber];
                }
            }
        }
        CFRelease(multiPhonesRef);
    }
    [self didSelectContactWithNumbers:personPhoneNumbers];
}

- (void)didSelectContactWithNumbers:(NSArray *)personPhoneNumbers
{
    __weak __typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(0.5f, NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        
        switch (personPhoneNumbers.count) {
                
            case 0: {
                
                [strongSelf showActionAlert:YES
                                  withTitle:@"Warning".localize
                                       body:@"No numbers found".localize
                               cancelButton:@"OK"
                               otherButtons:nil
                                 completion:NULL];
                
                break;
            }
            case 1:
                [strongSelf didSelectNumber:personPhoneNumbers.firstObject];
                break;
            default: {
                
                __weak DiallerViewController *weakSelfBlock = strongSelf;
                [strongSelf showActionAlert:NO
                                  withTitle:@"Select Number".localize
                                       body:nil
                               cancelButton:@"Cancel".localize
                               otherButtons:personPhoneNumbers
                                 completion:^(NSInteger buttonIndex, NSString *buttonTitle) {
                               if (kAlertCancelButtonIndex != buttonIndex) {
                                   __strong DiallerViewController *strongSelfBlock = weakSelfBlock;
                                   [strongSelfBlock didSelectNumber:buttonTitle];
                               }
                           }];
                break;
            }
        }
    });
}

- (void)didSelectNumber:(NSString *)number
{
    NSString *firstNumber = [number substringToIndex:1];
    if ([@"0" isEqualToString:firstNumber])
        number = [number substringFromIndex:1];
    self.numberLabel.text = number;
    [self.contactsDisplay removeAllObjects];
    [self.table reloadData];
}

#pragma mark - UIPicker Methods

- (void)didCancelCountriesPicker:(UIBarButtonItem *)sender
{
    [self.fakeTextField resignFirstResponder];
}

- (void)didDoneCountriesPicker:(UIBarButtonItem *)sender
{
    NSString *selectedCode = self.countries[[self.countriesPicker selectedRowInComponent:0]][kCountryCodeKey];
    NSString *selectedName = self.countries[[self.countriesPicker selectedRowInComponent:0]][kCountryNameKey];
    switch (self.countryType) {
        case SelectCountryTypeFrom:
            self.fromLabel.text = selectedName;
            [[NSUserDefaults standardUserDefaults] setObject:selectedCode forKey:kLastFromCountryCode];
            break;
        case SelectCountryTypeTo:
            self.toLabel.text = selectedName;
            [[NSUserDefaults standardUserDefaults] setObject:selectedCode forKey:kLastToCountryCode];
            break;
        default:
            break;
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self didCancelCountriesPicker:nil];
    [self fetchCountriesLabelsSizes];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.countries.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.countries[row][kCountryNameKey];
}

@end
