//
//  DialerViewController.h
//  DialerApp
//
//  Created by Valerii Lider on 4/7/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface DiallerViewController : BaseViewController

@property (nonatomic, strong) NSString *loginID;

@end
