//
//  LoginViewController.h
//  DialerApp
//
//  Created by Valerii Lider on 4/7/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface LoginViewController : BaseViewController

@end
