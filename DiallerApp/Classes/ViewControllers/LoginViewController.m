//
//  LoginViewController.m
//  DialerApp
//
//  Created by Valerii Lider on 4/7/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "LoginViewController.h"

#import "DiallerViewController.h"
#import "CodeScannerViewController.h"
#import "LoginTextView.h"

@interface LoginViewController () <UITextFieldDelegate, CodeScannerViewControllerDelegate>

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topOffset;
@property (nonatomic, weak) IBOutlet LoginTextView *loginTextView;
@property (nonatomic, weak) IBOutlet LoginTextView *passwordTextView;
@property (nonatomic, weak) IBOutlet UIButton *signInButton;
@property (nonatomic, weak) IBOutlet UILabel *signInLabel;

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set UI
    self.navigationController.navigationBarHidden = YES;
    if (isLessThan4Inch)
        self.topOffset.constant = 60.f;
    self.loginTextView.placeholder = @"LOGIN OR PHONE NUMBER".localize;
    self.loginTextView.background = [UIImage imageNamed:@"TextViewBack"];
    self.loginTextView.font = [UIFont systemFontOfSize:round(12.f*kScreenScaleFactor)];
    self.passwordTextView.placeholder = @"PASSWORD".localize;
    self.passwordTextView.background = [UIImage imageNamed:@"TextViewBack"];
    self.passwordTextView.font = [UIFont systemFontOfSize:round(12.f*kScreenScaleFactor)];
    self.signInButton.titleLabel.font = [UIFont systemFontOfSize:round(15.f*kScreenScaleFactor)];
    [self.signInButton setTitle:@"SIGN IN".localize forState:UIControlStateNormal];
    self.loginTextView.placeholder = @"SIGN UP".localize;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)didPressSignInButton:(UIButton *)sender
{
//    if ([@"" isEqualToString:self.loginTextView.text]) {
//        [self showActionAlert:YES
//                    withTitle:@"Warning".localize
//                         body:@"Access code field should not be empty".localize
//                 cancelButton:@"Cancel".localize
//                 otherButtons:nil completion:NULL];
//    } else
        [self performSegueWithIdentifier:@"showDialler" sender:nil];
}

- (IBAction)didPressScanCodeButton:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"showCodeScanner" sender:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - CodeScanner Delegate

- (void)onDidCaptureCode:(NSString *)code
{
//    self.accessCodeField.text = code;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([@"showCodeScanner" isEqualToString:segue.identifier]) {
        
        UINavigationController *navigation = segue.destinationViewController;
        CodeScannerViewController *controller = navigation.viewControllers.firstObject;
        controller.delegate = self;
    }
    if ([@"showDialler" isEqualToString:segue.identifier]) {
        
//        DiallerViewController *controller = segue.destinationViewController;
//        controller.loginID = self.accessCodeField.text;
    }
}

@end
