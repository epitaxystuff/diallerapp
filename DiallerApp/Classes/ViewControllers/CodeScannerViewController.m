//
//  CodeScannerViewController.m
//  DiallerApp
//
//  Created by Valerii Lider on 4/8/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "CodeScannerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface CodeScannerViewController () <AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, weak) IBOutlet UIView *previewView;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *startButton;

@property (nonatomic, assign) BOOL isReading;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@end

@implementation CodeScannerViewController

- (void)dealloc
{
    [self stopSession];
    self.videoPreviewLayer = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isReading = NO;
    self.captureSession = nil;
    
    self.statusLabel.text = @"Tap \"Start\" to scan".localize;
    
    [self startReading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)didPressCancelButton:(UIBarButtonItem *)sender
{
    [self stopSession];
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)startStopReading:(UIBarButtonItem *)sender
{
    if (!self.isReading) {
        [self.startButton setTitle:@"Stop".localize];
        self.statusLabel.text = @"Scanning for QR Code…".localize;
    } else {
        [self.startButton setTitle:@"Start".localize];
        self.statusLabel.text = @"Tap \"Start\" to scan".localize;
    }
        
    
    self.isReading = !self.isReading;
}

#pragma mark - Private Methods

- (BOOL)startReading
{
    NSError *error = nil;
    
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    self.captureSession = [[AVCaptureSession alloc] init];
    [self.captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [self.captureSession addOutput:captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue = dispatch_queue_create("scanQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    
    self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.videoPreviewLayer setFrame:self.previewView.layer.bounds];
    [self.previewView.layer addSublayer:self.videoPreviewLayer];
    
    [self.captureSession startRunning];
    
    return YES;
}

- (void)stopSession
{
    if (self.captureSession) {
        [self.captureSession stopRunning];
        self.captureSession = nil;
    }
    
    [self.videoPreviewLayer removeFromSuperlayer];
}

#pragma mark - Capture Delegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    if (self.isReading && nil != metadataObjects && 0 < metadataObjects.count) {
        
        AVMetadataMachineReadableCodeObject *metadataObj = metadataObjects.firstObject;
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            __weak CodeScannerViewController *weakSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                
                __strong CodeScannerViewController *strongSelf = weakSelf;
                AudioServicesPlaySystemSound(1117);
                
                [strongSelf stopSession];
                if ([strongSelf.delegate respondsToSelector:@selector(onDidCaptureCode:)])
                    [strongSelf.delegate onDidCaptureCode:metadataObj.stringValue];
                strongSelf.isReading = NO;
                
                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            });
        }
    }
}

@end
