//
//  ContactCellTableViewCell.m
//  DiallerApp
//
//  Created by Valerii Lider on 5/15/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

- (void)awakeFromNib
{
    self.avatar.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatar.layer.borderWidth = 1.f;
    self.avatar.layer.cornerRadius = self.avatar.bounds.size.width/2;
    self.avatar.clipsToBounds = YES;
}

@end
