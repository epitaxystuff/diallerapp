//
//  LoginTextView.m
//  DiallerApp
//
//  Created by Valerii Lider on 5/14/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "LoginTextView.h"

@implementation LoginTextView

- (void)drawPlaceholderInRect:(CGRect)rect {
    
    UIColor *color = [UIColor colorWithRed:92.f/255 green:110.f/255 blue:159.f/255 alpha:1.f];
    if ([self.placeholder respondsToSelector:@selector(drawInRect:withAttributes:)]) {
        
        NSDictionary *attributes = @{NSForegroundColorAttributeName:color, NSFontAttributeName:self.font};
        CGRect boundingRect = [self.placeholder boundingRectWithSize:rect.size options:0 attributes:attributes context:nil];
        boundingRect = CGRectInset(boundingRect, 0.f, -2.f);
        [self.placeholder drawAtPoint:CGPointMake(0.f, (rect.size.height/2)-boundingRect.size.height/2) withAttributes:attributes];
    }
}

//override func borderRectForBounds(bounds: CGRect) -> CGRect {
//    
//    return super.borderRectForBounds(bounds)
//}


//override func textRectForBounds(bounds: CGRect) -> CGRect {
//    
//    let leftMargin: CGFloat = 16
//    let viewWidth: CGFloat = self.leftView?.bounds.width ?? 0
//    let viewRightMargin: CGFloat = 0
//    let rightTextMargin: CGFloat = 8
//    let rightViewMargin: CGFloat = 16
//    return CGRectMake(leftMargin + viewWidth + viewRightMargin, 0, bounds.width - leftMargin - viewWidth - viewRightMargin - clearButtonRectForBounds(bounds).width - rightViewMargin - rightTextMargin, bounds.height)
//}
//
//override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
//    
//    return self.textRectForBounds(bounds)
//}
//
//override func editingRectForBounds(bounds: CGRect) -> CGRect {
//    
//    return self.textRectForBounds(bounds)
//}

- (CGRect)placeholderRectForBounds:(CGRect)bounds
{
    bounds.size.width -= 12.f;
    return CGRectInset(bounds, 5.f, 7.f);
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    bounds.size.width -= 12.f;
    return CGRectInset(bounds, 5.f, 5.f);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    bounds.size.width -= 12.f;
    return CGRectInset(bounds, 5.f, 5.f);
}

@end
