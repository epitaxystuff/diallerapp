//
//  ContactCellTableViewCell.h
//  DiallerApp
//
//  Created by Valerii Lider on 5/15/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *avatar;
@property (nonatomic, weak) IBOutlet UILabel *contactNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *contactPhoneLabel;

@end
