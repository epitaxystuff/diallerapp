//
//  PadNumberView.h
//  DiallerApp
//
//  Created by Valerii Lider on 5/15/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PadNumberViewDelegate <NSObject>

@required
- (void)padNumberTappedWithTag:(NSInteger)tag isLongPress:(BOOL)longPress;
- (void)padNumberLongPressed;

@end

@interface PadNumberView : UIView

@property (nonatomic, assign) id <PadNumberViewDelegate> delegate;

@end
