
//
//  PadNumberView.m
//  DiallerApp
//
//  Created by Valerii Lider on 5/15/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "PadNumberView.h"

@interface PadNumberView ()

@property (nonatomic, assign) BOOL pressed;

@end

@implementation PadNumberView

- (void)awakeFromNib
{
    self.pressed = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    [self addGestureRecognizer:tapGesture];
    
    if (PadButtonTagZero == self.tag) {
        UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didLongPress:)];
        [self addGestureRecognizer:longPressGesture];
    }
}

#pragma mark - IBActions

- (void)didTap:(UITapGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            break;
        case UIGestureRecognizerStateChanged:
            break;
        case UIGestureRecognizerStateEnded:
        {
            self.pressed = YES;
            [self setNeedsDisplay];
            if ([self.delegate respondsToSelector:@selector(padNumberTappedWithTag:isLongPress:)])
                [self.delegate padNumberTappedWithTag:self.tag isLongPress:NO];
            __weak __typeof(self) weakSelf = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.pressed = NO;
                [strongSelf setNeedsDisplay];
            });
            break;
        }
        default:
            break;
    }
}

- (void)didLongPress:(UILongPressGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            self.pressed = YES;
            [self setNeedsDisplay];
            if ([self.delegate respondsToSelector:@selector(padNumberTappedWithTag:isLongPress:)])
                [self.delegate padNumberTappedWithTag:self.tag isLongPress:YES];
            break;
        case UIGestureRecognizerStateChanged:
            break;
        case UIGestureRecognizerStateEnded:
            self.pressed = NO;
            [self setNeedsDisplay];
            if ([self.delegate respondsToSelector:@selector(padNumberLongPressed)])
                [self.delegate padNumberLongPressed];
            break;
        default:
            break;
    }
}

#pragma mark - Internal Method

- (void)drawRect:(CGRect)rect
{
    if (self.pressed) {
        [[UIColor colorWithWhite:0.7 alpha:0.5] setFill];
        UIRectFillUsingBlendMode( rect , kCGBlendModeDarken );
    } else {
        [[UIColor colorWithWhite:1.0 alpha:1.] setFill];
        UIRectFillUsingBlendMode( rect , kCGBlendModeNormal );
    }
}

@end
