//
//  Loader.m
//  DiallerApp
//
//  Created by Valerii Lider on 4/21/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "Loader.h"

@implementation Loader

- (id)initWithSize:(CGFloat)size center:(CGPoint)center
{
    self = [super initWithFrame:CGRectMake(center.x - size/2, center.y - size/2, size, size)];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.65f];
        self.layer.cornerRadius = 45.f;
        NSMutableArray *animationImages = [@[[UIImage imageNamed:@"loader0"]] mutableCopy];
        for (NSInteger i = 1; i <= 71; i++)
            [animationImages addObject:[UIImage imageNamed:[NSString stringWithFormat:@"loader%ld", (long)i]]];
        self.animationImages = animationImages;
        self.animationDuration = 2.2f;
        self.animationRepeatCount = 0;
    }
    return self;
}

@end
