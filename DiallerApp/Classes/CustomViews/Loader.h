//
//  Loader.h
//  DiallerApp
//
//  Created by Valerii Lider on 4/21/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Loader : UIImageView

- (id)initWithSize:(CGFloat)size center:(CGPoint)center;

@end
