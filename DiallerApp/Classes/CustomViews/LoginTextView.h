//
//  LoginTextView.h
//  DiallerApp
//
//  Created by Valerii Lider on 5/14/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginTextView : UITextField

@end
