//
//  Defines.h
//  DiallerApp
//
//  Created by Valerii Lider on 4/7/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#ifndef DiallerApp_Defines_h
#define DiallerApp_Defines_h

#pragma mark - Keys

#define kCountryNameKey                 @"name"
#define kCountryCodeKey                 @"code"

/*-----------------------------------------------*/

#pragma mark - Type Defines

typedef enum
{
    PadButtonTagAsterisk = 11,
    PadButtonTagZero = 10,
    PadButtonTagSharp = 12
} PadButtonTag;
/*-----------------------------------------------*/
#pragma mark - UserDefaults

#define kLastFromCountryCode            @"fromCountryCode"
#define kLastToCountryCode              @"toCountryCode"

/*-----------------------------------------------*/
#pragma mark - General Macroses

#define isLessThan4Inch                 MAX([UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width) < 568.f
#define kScreenWidth                    MIN([UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width)
#define kScreenScaleFactor              kScreenWidth/320.f

#endif
