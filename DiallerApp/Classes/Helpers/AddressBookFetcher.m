//
//  AddressBookFetcher.m
//  DiallerApp
//
//  Created by Valerii Lider on 5/15/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "AddressBookFetcher.h"
#import <AddressBookUI/AddressBookUI.h>

@interface AddressBookFetcher ()

@property (nonatomic, strong) NSMutableArray *contacts;

@property (strong, nonatomic) NSManagedObjectContext *masterManagedObjectContext;

@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation AddressBookFetcher

+ (AddressBookFetcher *)sharedObject
{
    static dispatch_once_t pred;
    static id shared = nil;
    dispatch_once(&pred, ^{
        shared = [[super alloc] init];
    });
    return shared;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        
        self.contacts = [@[] mutableCopy];
    }
    return self;
}

- (void)loadContactsWithCompletion:(void(^)())completion
{
    [self.contacts removeAllObjects];
    CFErrorRef myError = NULL;
    ABAddressBookRef myAddressBook = ABAddressBookCreateWithOptions(NULL, &myError);
    ABAddressBookRequestAccessWithCompletion(myAddressBook, ^(bool granted, CFErrorRef error) {
        
        if (granted) {
            CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(myAddressBook);
            
            for (NSInteger i = 0; i < CFArrayGetCount(allPeople); ++i) {
                
                ABRecordRef contactPerson = CFArrayGetValueAtIndex(allPeople, i);
                
                BOOL addPerson = NO;
                NSMutableDictionary *person = [@{} mutableCopy];
                
                ABMultiValueRef multiPhonesRef = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty);
                if (multiPhonesRef) {
                    if (0 != ABMultiValueGetCount(multiPhonesRef)) {
                        
                        NSMutableArray *personPhoneNumbers = [@[] mutableCopy];
                        for (NSInteger i = 0; i < ABMultiValueGetCount(multiPhonesRef); i++) {
                            
                            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhonesRef, i);
                            NSString *phoneNumber = (__bridge_transfer NSString *)phoneNumberRef;
                            
                            if (![@"" isEqualToString:phoneNumber]) {
                                
                                NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@" -()"];
                                phoneNumber = [[phoneNumber componentsSeparatedByCharactersInSet:doNotWant] componentsJoinedByString:@""];
                                phoneNumber = [phoneNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
                                NSString *phoneRegex = @"^(\\+?)(\\d{1,17})$";
                                NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
                                if ([phoneTest evaluateWithObject:phoneNumber])
                                    [personPhoneNumbers addObject:phoneNumber];
                            }
                        }
                        if (0 != personPhoneNumbers.count) {
                            addPerson = YES;
                            [person setValue:personPhoneNumbers forKey:kContactPhonesKey];
                        }
                    }
                    CFRelease(multiPhonesRef);
                }
                
                if (addPerson) {
                    
                    CFStringRef firstNameRef = ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
                    CFStringRef lastNameRef = ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
                    NSString *firstName = (__bridge_transfer NSString *)firstNameRef;
                    NSString *lastName =  (__bridge_transfer NSString *)lastNameRef;
                    NSString *fullName = nil;
                    if (nil == firstName && nil == lastName)
                        fullName = @"Unknown Contact";
                    if (nil != firstName && nil == lastName)
                        fullName = firstName;
                    if (nil == firstName && nil != lastName)
                        fullName = lastName;
                    if (nil != firstName && nil != lastName)
                        fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
                    person[kContactNameKey] = fullName;
                    
                    CFDataRef avatarRef = ABPersonCopyImageDataWithFormat(contactPerson, kABPersonImageFormatThumbnail);
                    if (NULL != avatarRef) {
                        UIImage *avatar = [UIImage imageWithData:(__bridge_transfer NSData *)avatarRef];
                        person[kContactAvatarKey] = avatar;
                    }
                    [[AddressBookFetcher sharedObject].contacts addObject:person];
                }
            }
            completion();
        } else
            completion();
    });
}

@end
