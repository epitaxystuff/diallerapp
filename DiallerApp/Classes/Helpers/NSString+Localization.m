//
//  NSString+Localization.m
//  DiallerApp
//
//  Created by Valerii Lider on 4/8/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import "NSString+Localization.h"

@implementation NSString (Localization)

- (NSString *)localize
{
    NSString *languageCode = [[NSLocale preferredLanguages] firstObject];
    return NSLocalizedStringFromTable(self, languageCode, nil);
}

@end
