//
//  AddressBookFetcher.h
//  DiallerApp
//
//  Created by Valerii Lider on 5/15/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kContactNameKey                 @"contactName"
#define kContactAvatarKey               @"contactAvatar"
#define kContactPhonesKey               @"contactPhones"

@interface AddressBookFetcher : NSObject

@property (nonatomic, readonly, strong) NSMutableArray *contacts;

+ (AddressBookFetcher *)sharedObject;
- (void)loadContactsWithCompletion:(void(^)())completion;

@end
