//
//  NSString+Localization.h
//  DiallerApp
//
//  Created by Valerii Lider on 4/8/15.
//  Copyright (c) 2015 Spire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Localization)

- (NSString *)localize;

@end
